
var $ = require("../../../config/node_modules/jquery");

$(document).ready( function () {
    if(parent.document.getElementById('config-bar')) {
        $('html').addClass('config-mode');
    } else {
    }
    if ($("#colors").length > 0){ 
        rowColorFill();
    }
    if ($(".password-check").length > 0){ 
        showPassword()
    }
    
})

$(".scrolldown").click(function() {
  
    $(".scrolldown").click(function() {
        $('html,body').animate({
            scrollTop: $("section").first().offset().top
        }, 'slow');
    });
    
});

function rowColorFill() {
    var rowColors = $('#colors').attr('data-color');
    var rowColorsSingle = rowColors.split('||');

    console.log(rowColorsSingle);

	$('.project-tray .plate--row').each(function(i){
        var rowColorClass = 'color__background';
        $(this).append('<div class="' + rowColorClass + '"></div>');
        
        console.log($(this).find('.color__background').css('background', rowColorsSingle[i]))    
    });
}

$(".thumbnail").click(function() {
    $(this).addClass('active-vid')
});

export const setActive = (el, active) => {
    const formField = el.parentNode;
    if (active) {
        formField.classList.add('form-field--is-active');
    } else {
        formField.classList.remove('form-field--is-active');
        el.value === '' ?
            formField.classList.remove('form-field--is-filled') :
            formField.classList.add('form-field--is-filled');
    }
};

