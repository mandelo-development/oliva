// Import Swiper and modules
import {
	Swiper,
	Navigation,
	Pagination,
	Scrollbar,
	Controller,
	Mousewheel,
	Autoplay,
	Thumbs
} from '../../../config/node_modules/swiper/swiper.esm';
Swiper.use([Navigation, Pagination, Scrollbar, Controller, Mousewheel, Autoplay, Thumbs]);

var proj_swiper = new Swiper('.project_slider', {
	slidesPerView: 1,
	spaceBetween: 40,
	scrollbar: {
	  el: '.swiper-scrollbar',
	  hide: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 3,
			slidesPerGroup: 3,
        },
	}
});
var rela_swiper = new Swiper('.related__products', {
	slidesPerView: 1.2,
	spaceBetween: 40,
	navigation: {
        nextEl: '.rel-btn-next',
        prevEl: '.rel-btn-prev',
	},
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 3,
			slidesPerGroup: 1,
        },
	}
});

var prod_swiper_thumbs = new Swiper('.product_slider_thumbs', {
	slidesPerView: 4,
	spaceBetween: 20,
	watchSlidesVisibility: true,
	watchSlidesProgress: true,
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 3,
			slidesPerGroup: 30,
        },
	},
});

var prod_swiper = new Swiper('.product_slider', {
	slidesPerView: 1,
	spaceBetween: 40,
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 1,
			slidesPerGroup: 1,
        },
	},
	thumbs: {
		swiper: prod_swiper_thumbs
	}
});




var tesm_swiper = new Swiper('.testimonial_slider', {
	slidesPerView: 1,
	spaceBetween: 40,
	slidesPerGroup: 1,
	loopFillGroupWithBlank: false,
	pagination: {
	  el: '.swiper-pagination',
	  clickable: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        768: {
			slidesPerView: 3,
			
        },
	}
	
});

var usp_slider = new Swiper('.usp_slider', {
	slidesPerView: 1,
	spaceBetween: 40,
	pagination: {
	  el: '.swiper-pagination',
	  clickable: true,
	},
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
	},
	mousewheel: {
		forceToAxis: true, 
	},
	autoplay: {
        delay: 3000,
	},
	loop: true
}); 

var head_swiper = new Swiper('.header_slider', {
	slidesPerView: 1,
	spaceBetween: 0,
	pagination: {
	  el: '.swiper-pagination',
	  clickable: true,
	},
	mousewheel: {
		forceToAxis: true, 
	},
	autoplay: {
        delay: 8000,
	},
	loop: true
});

var imga_swiper = new Swiper('.images_slider', {
	slidesPerView: 1,
	spaceBetween: 40,
	pagination: {
		el: '.swiper-pagination',
		clickable: true,
	},
	navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
	},
	mousewheel: {
		forceToAxis: true, 
	},
});

var cat_swiper = new Swiper('.categories-links', {
	slidesPerView: 1.5,
	spaceBetween: 40,
	mousewheel: {
		forceToAxis: true, 
	},
	breakpoints: {
        680: {
			slidesPerView: 3,
		},
		800: {
			slidesPerView: 3.5,	
		},
		1020: {
			slidesPerView: 5,
		},
	}
});



