
import {setActive} from './functions';
var $ = require("../../../config/node_modules/jquery");

async function general() {
    $(document).ready( function () {
        /* FORM LABELS */ 
        [].forEach.call(
            document.querySelectorAll('.form-field__label, product__search .input, .form-field input, .form-field textarea, .form-field__textarea'),
            el => {
                el.onblur = () => {
                    setActive(el, false);
        
                };
                el.onfocus = () => {
                    setActive(el, true);
                };
        });

        let cta = $('.cta--align-others');
        cta.each(function(){
            $(this).parent().addClass('plate--element__cta--align');
        });  
     
        $(".holder a:not('.jp-current')").on("click", function () {
            $('html,body').animate({
            scrollTop: 275
            }, 800);
        });
    });
   
    $(".button button, button, .button input").click(function(e){
        if ($(this).hasClass("clicked")){
            e.preventDefault();
        } else {
            $(this).parent('.button').addClass("clicked");
        }
    });

    setInterval(function() {
        $('.clicked').removeClass('clicked')
    }, 10000)
    let body = document.querySelector('body');
  
    $('.product__search__input svg').click(function(){
        if($('.product__search__input .input').hasClass('hide'))
        {
           $('.product__search__input .input').removeClass('hide');
        }
        else
        {
           $('.product__search__input .input').addClass('hide');
        }
     
     });

    $(document).click(function(e) {
       
        if(body.classList.contains("shop-dropdown-show")){
            var targetItem = e.target.classList;
            if (targetItem.contains("body")){
                body.classList.remove('shop-dropdown-show');
            }
        }
    });

  
    
} export {
    general
}
