var $ = require("../../../../config/node_modules/jquery");
import {
	xhrRequest
} from './functions/request'
import {
	range
} from './functions/range'
import {
	elementInViewport
} from './functions/elementinviewport'
import {
	pcAddToCart,
	pcRemoveFromCart
}
from './pc-cart'
import {
	pcProduct
} from './pc-product'

let resultElement = document.querySelector('.pc__index__result');
// Base array with all parameters
let filterParameters = {};
let filtering = false;
let pageEnd = false;
let sortSelector = document.querySelector('.pc__index__result__sort');
let prevParameters;
filterParameters['page'] = 1;

async function pcFilters() {
	if (document.querySelector('.pc__index__filters')) {
		if (sortSelector) {
			filterParameters['sort_type'] = sortSelector.value;
			// Change sort option
			sortSelector.addEventListener('change', function() {
				filterParameters['sort_type'] = sortSelector.value;
				filterParameters['page'] = 1;
				updateResult(filterParameters)
			})
		}

		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.display === "block") {
			panel.style.display = "none";
			} else {
			panel.style.display = "block";
			}
		});
		}

		// Check Attrutes filters, add to filterParameters if checked
		filterParameters['category'] = document.querySelector('.pc__index').dataset.id;
		let attributeNames = document.querySelectorAll('.pc__index__filters__attributes');
		if (attributeNames.length > 0) {
			filterParameters['attributenames'] = attributeNames[0].dataset.attributenames.split('|')
			let allFilterInputs = document.querySelectorAll('.pc__index__filters__tax__items__item__input');
			allFilterInputs.forEach((input, i) => {
				filterParameters[input.dataset.parent] = [];
				checkIfChecked(input);
				input.addEventListener('change', function() {
					filterParameters['page'] = 1;
					let thisInput = this;
					checkIfChecked(thisInput);
					updateResult(filterParameters)
					console.log(filterParameters[thisInput.dataset.parent])		
				})
			});

		}


		function checkIfChecked(thisInput) {
			if (thisInput.checked) {
				filterParameters[thisInput.dataset.parent] = [];
				filterParameters[thisInput.dataset.parent].push(thisInput.dataset.id);
				for (let sibling of thisInput.parentElement.parentElement.parentElement.children) {
					sibling.classList.remove('active');
					let siblingInput = sibling.querySelector('input');
					if (siblingInput.getAttribute('id') != thisInput.getAttribute('id')){
						siblingInput.checked = false;
					}
				}
				thisInput.parentElement.parentElement.classList.add('active');
			} else {
				const index = filterParameters[thisInput.dataset.parent].indexOf(thisInput.dataset.id);
				if (index > -1) {
					filterParameters[thisInput.dataset.parent].splice(index, 1);
				}
				thisInput.parentElement.parentElement.classList.remove('active');
			}
		}

		// Pricefilter
		let priceFilter = document.querySelector('.pc__index__filters__price');
		if (priceFilter) {
			priceFilter.querySelectorAll('input').forEach((rangeInput, i) => {
				rangeInput.addEventListener('change', function() {
					filterParameters['page'] = 1;
					if (priceFilter.querySelector('.filter__input__range__values__min')) {
						filterParameters['price_min'] = priceFilter.querySelector('.filter__input__range__values__min').value;
						filterParameters['price_max'] = priceFilter.querySelector('.filter__input__range__values__max').value;
					} else {
						filterParameters['price_min'] = priceFilter.querySelector('.filter__input__range__selector__min').value;
						filterParameters['price_max'] = priceFilter.querySelector('.filter__input__range__selector__max').value;
					}
					updateResult(filterParameters);
				})
			});
		}

		pagination()
	}
}



// Pagination
function pagination() {
	let pageLinks = resultElement.querySelectorAll('.pagination__item');
	if (pageLinks[0]) {
		pageLinks.forEach((page, i) => {
			page.addEventListener('click', function() {
				let indexOffset = document.querySelector('.pc__index').offsetTop;
				console.log(indexOffset);
				filterParameters['page'] = this.dataset.targetindex;
				updateResult(filterParameters, false, 'pageChanged', {
					'page': filterParameters['page']
				});

				$('html,body').animate({
					scrollTop: indexOffset
				}, 800);
			})
		});
	} else if (resultElement.dataset.pagination == 'lazy') {
		document.addEventListener('scroll', function() {
			let resultProducts = resultElement.querySelectorAll(".pc__product");
			let lastProduct = resultProducts[resultProducts.length - 1];
			if (elementInViewport(lastProduct, 100) && !filtering && !pageEnd) {
				filtering = true;
				filterParameters['page'] = filterParameters['page'] + 1;
				updateResult(filterParameters, true, 'pageChanged', {
					'page': filterParameters['page']
				});
			}
		})
	}
}

// Function that updates result
function updateResult(parameters, append, eventName, eventParameters) {
	console.log(eventName);
	resultElement.classList.add('pc__index__result--loading');
	let result;
	async function request() {
		if (resultElement.dataset.url_parameters == "true") {
			result = await xhrRequest('platecommerce', parameters, true);
		} else {
			result = await xhrRequest('platecommerce', parameters, false);
		}
	}
	request().then(function() {
		if (result.includes('THE-END-PAGE')) {
			pageEnd = true;
		} else {
			pageEnd = false;
		}
		if (append) {
			resultElement.innerHTML = resultElement.innerHTML + result;
		} else {
			resultElement.innerHTML = result;
		}
		resultElement.classList.remove('pc__index__result--loading');
		pagination();
		pcAddToCart(resultElement, true);
		pcRemoveFromCart(resultElement);
		pcProduct(resultElement)
		filtering = false;
		prevParameters = parameters
		if (eventName) {
			let eventResult = new CustomEvent(eventName, {
				"detail": eventParameters
			});
			document.dispatchEvent(eventResult);
		}
	})
}

export {
	pcFilters
}
