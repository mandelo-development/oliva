
import '../../../../config/node_modules/regenerator-runtime/runtime'
import {
	PlateCommerce
} from './functions/PlateCommerce'
let {
	Cart,
	OrderHandler,
	Config,
	Order
} = PlateCommerce

import {
	pcCheckout,
	pcShipping,
	succesfullOrder
} from './pc-checkout'

import {
	pcEvents
} from './pc-events'

import {
	pcAddToCart,
	cartSize
} from './pc-cart'

import {
	pcTotal
} from './pc-total'

import {
	pcProduct
} from './pc-product'

import {
	pcFilters
} from './pc-filters'

import {
	range
} from './functions/range'

import {
	pcCoupons
} from './pc-coupons'

if (succesfullOrder()) {
	Cart.reset();
}

export function platecommerce() {
	Config.siteTranslationId = document.querySelector('body').dataset.site_translation_id;
	Config.siteTranslationLanguage = document.querySelector('body').dataset.site_translation_language;
	pcEvents();
	pcAddToCart(document);
	cartSize()
	pcTotal();
	pcProduct(document);
	pcFilters();
	pcCheckout();
	pcShipping();
	pcCoupons();

	if (document.querySelector('.filter__input__range__selector')) {
		range()
	}
}

/* Styles */
import '../../styles/style';
