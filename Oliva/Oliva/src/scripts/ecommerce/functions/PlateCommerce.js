export var PlateCommerce = function(t) {
	var e = {};

	function r(n) {
		if (e[n]) return e[n].exports;
		var o = e[n] = {
			i: n,
			l: !1,
			exports: {}
		};
		return t[n].call(o.exports, o, o.exports, r), o.l = !0, o.exports
	}
	return r.m = t, r.c = e, r.d = function(t, e, n) {
		r.o(t, e) || Object.defineProperty(t, e, {
			enumerable: !0,
			get: n
		})
	}, r.r = function(t) {
		"undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
			value: "Module"
		}), Object.defineProperty(t, "__esModule", {
			value: !0
		})
	}, r.t = function(t, e) {
		if (1 & e && (t = r(t)), 8 & e) return t;
		if (4 & e && "object" == typeof t && t && t.__esModule) return t;
		var n = Object.create(null);
		if (r.r(n), Object.defineProperty(n, "default", {
				enumerable: !0,
				value: t
			}), 2 & e && "string" != typeof t)
			for (var o in t) r.d(n, o, function(e) {
				return t[e]
			}.bind(null, o));
		return n
	}, r.n = function(t) {
		var e = t && t.__esModule ? function() {
			return t.default
		} : function() {
			return t
		};
		return r.d(e, "a", e), e
	}, r.o = function(t, e) {
		return Object.prototype.hasOwnProperty.call(t, e)
	}, r.p = "", r(r.s = 12)
}([function(t, e, r) {
	"use strict";
	var n = r(3),
		o = Object.prototype.toString;

	function i(t) {
		return "[object Array]" === o.call(t)
	}

	function s(t) {
		return void 0 === t
	}

	function a(t) {
		return null !== t && "object" == typeof t
	}

	function c(t) {
		if ("[object Object]" !== o.call(t)) return !1;
		var e = Object.getPrototypeOf(t);
		return null === e || e === Object.prototype
	}

	function u(t) {
		return "[object Function]" === o.call(t)
	}

	function d(t, e) {
		if (null != t)
			if ("object" != typeof t && (t = [t]), i(t))
				for (var r = 0, n = t.length; r < n; r++) e.call(null, t[r], r, t);
			else
				for (var o in t) Object.prototype.hasOwnProperty.call(t, o) && e.call(null, t[o], o, t)
	}
	t.exports = {
		isArray: i,
		isArrayBuffer: function(t) {
			return "[object ArrayBuffer]" === o.call(t)
		},
		isBuffer: function(t) {
			return null !== t && !s(t) && null !== t.constructor && !s(t.constructor) && "function" == typeof t.constructor.isBuffer && t.constructor.isBuffer(t)
		},
		isFormData: function(t) {
			return "undefined" != typeof FormData && t instanceof FormData
		},
		isArrayBufferView: function(t) {
			return "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(t) : t && t.buffer && t.buffer instanceof ArrayBuffer
		},
		isString: function(t) {
			return "string" == typeof t
		},
		isNumber: function(t) {
			return "number" == typeof t
		},
		isObject: a,
		isPlainObject: c,
		isUndefined: s,
		isDate: function(t) {
			return "[object Date]" === o.call(t)
		},
		isFile: function(t) {
			return "[object File]" === o.call(t)
		},
		isBlob: function(t) {
			return "[object Blob]" === o.call(t)
		},
		isFunction: u,
		isStream: function(t) {
			return a(t) && u(t.pipe)
		},
		isURLSearchParams: function(t) {
			return "undefined" != typeof URLSearchParams && t instanceof URLSearchParams
		},
		isStandardBrowserEnv: function() {
			return ("undefined" == typeof navigator || "ReactNative" !== navigator.product && "NativeScript" !== navigator.product && "NS" !== navigator.product) && ("undefined" != typeof window && "undefined" != typeof document)
		},
		forEach: d,
		merge: function t() {
			var e = {};

			function r(r, n) {
				c(e[n]) && c(r) ? e[n] = t(e[n], r) : c(r) ? e[n] = t({}, r) : i(r) ? e[n] = r.slice() : e[n] = r
			}
			for (var n = 0, o = arguments.length; n < o; n++) d(arguments[n], r);
			return e
		},
		extend: function(t, e, r) {
			return d(e, (function(e, o) {
				t[o] = r && "function" == typeof e ? n(e, r) : e
			})), t
		},
		trim: function(t) {
			return t.replace(/^\s*/, "").replace(/\s*$/, "")
		},
		stripBOM: function(t) {
			return 65279 === t.charCodeAt(0) && (t = t.slice(1)), t
		}
	}
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	}), e.Cart = void 0;
	const n = r(2);
	class o {
		static getOrder() {
			let t = new n.default(this._getItems());
			return t.shippingMethod = this.getShippingMethodId(), t.couponCodes = this.getDiscountCouponCodes(), t.email = this.getEmail(), t.shippingAddress = this.getShippingAddress(), t.billingAddress = this.getBillingAddress(), t.paymentProvider = this.getPaymentProvider(), t.paymentProviderParameters = this.getPaymentProviderParameters(), t.redirectUrl = this.getRedirectUrl(), t.notes = this.getNotes(), t
		}
		static addItem(t) {
			let e = ["id", "quantity"];
			for (let r of e)
				if (!t[r]) throw new Error(`Missing parameter '${r}'`);
			let r = this._getItems(),
				n = this._findMatchingItem(t, r);
			return n ? n.quantity += t.quantity : (t.uuid = this._generateuuid(), r.push(t)), this._setItems(r), t
		}
		static getItems() {
			return this._getItems()
		}
		static updateItem(t, e) {
			let r = this._getItems(),
				n = r.find(e => e.uuid == t);
			return n && Object.assign(n, e), this._setItems(r), n
		}
		static deleteItem(t) {
			let e = this._getItems().filter(e => e.uuid != t);
			return this._setItems(e), e
		}
		static deleteAllItems() {
			return this._setItems([])
		}
		static getDiscountCouponCodes() {
			return this._getDiscountCouponCodes()
		}
		static setShippingMethodId(t) {
			if (!t) throw new Error("Invalid Shipping Method Id: " + t);
			return localStorage.setItem(this._shippingMethodLocationName, t), t
		}
		static getShippingMethodId() {
			return localStorage.getItem(this._shippingMethodLocationName) || ""
		}
		static setNotes(t) {
			if (!t) throw new Error("Invalid Shipping Method Id: " + t);
			return localStorage.setItem(this._notesLocationName, t), t
		}
		static getNotes() {
			return localStorage.getItem(this._notesLocationName) || ""
		}
		static setRedirectUrl(t) {
			if (!t) throw new Error("Invalid redirectUrl: " + t);
			return localStorage.setItem(this._redirectUrlLocationName, t), t
		}
		static getRedirectUrl() {
			return localStorage.getItem(this._redirectUrlLocationName) || ""
		}
		static setPaymentProvider(t) {
			if (!t || "string" != typeof t) throw new Error("Invalid Payment Provider: " + t);
			return localStorage.setItem(this._paymentProviderLocationName, t), t
		}
		static getPaymentProvider() {
			return localStorage.getItem(this._paymentProviderLocationName) || ""
		}
		static setPaymentProviderParameters(t) {
			if (!t || "object" != typeof t) throw new Error("Invalid Payment Provider: " + t);
			return localStorage.setItem(this._paymentProviderParametersLocationName, JSON.stringify(t)), t
		}
		static getPaymentProviderParameters() {
			let t = localStorage.getItem(this._paymentProviderParametersLocationName) || "";
			return t ? JSON.parse(t) : {}
		}
		static setEmail(t) {
			if (!t) throw new Error("Invalid email: " + t);
			return localStorage.setItem(this._contactInformationLocationName("email"), t), t
		}
		static getEmail() {
			return localStorage.getItem(this._contactInformationLocationName("email")) || ""
		}
		static addDiscountCouponCode(t) {
			if (!t) throw new Error("Invalid Discount Coupon Code provided: " + t);
			let e = this._getDiscountCouponCodes();
			return e.push(t), e = [...new Set(e)], this._setDiscountCouponCodes(e), e
		}
		static deleteDiscountCouponCode(t) {
			if (!t) throw new Error("Invalid Discount Coupon Code provided: " + t);
			let e = this._getDiscountCouponCodes().filter(e => e == t);
			return this._setDiscountCouponCodes(e), e
		}
		static getShippingAddress() {
			return this._getAddress("shipping")
		}
		static getBillingAddress() {
			return this._getAddress("billing")
		}
		static setShippingAddress(t, e = !1) {
			e || this._validateAddress(t), this._setAddress("shipping", t)
		}
		static setBillingAddress(t, e = !1) {
			e || this._validateAddress(t), this._setAddress("billing", t)
		}
		static _validateAddress(t) {
			let e = ["name", "street", "city", "postalCode"];
			for (let r of e)
				if (!t[r]) throw new Error(`Missing parameter '${r}'`)
		}
		static _setAddress(t, e) {
			localStorage.setItem(this._contactInformationLocationName(t), JSON.stringify(e))
		}
		static _getAddress(t) {
			let e = localStorage.getItem(this._contactInformationLocationName(t));
			return e ? JSON.parse(e) : void 0
		}
		static deleteAllDiscountCouponCodes() {
			return this._setDiscountCouponCodes([])
		}
		static reset() {
			[this._shippingMethodLocationName, this._paymentProviderLocationName, this._discountCouponCodesLocationName, this._itemsLocationName, this._redirectUrlLocationName, this._notesLocationName, this._contactInformationLocationName("billing"), this._contactInformationLocationName("shipping"), this._contactInformationLocationName("email")].forEach(t => {
				console.log(t), localStorage.setItem(t, "")
			})
		}
		static _getItems() {
			let t = localStorage.getItem(this._itemsLocationName) || "[]";
			return JSON.parse(t)
		}
		static _setItems(t) {
			return localStorage.setItem(this._itemsLocationName, JSON.stringify(t)), t
		}
		static _findMatchingItem(t, e) {
			return e.find((function(e) {
				return e.id == t.id && e.productVariationId == t.productVariationId && JSON.stringify(e.selectedOptions) == JSON.stringify(t.selectedOptions)
			}))
		}
		static _getDiscountCouponCodes() {
			let t = localStorage.getItem(this._discountCouponCodesLocationName) || "[]";
			return JSON.parse(t)
		}
		static _setDiscountCouponCodes(t) {
			return localStorage.setItem(this._discountCouponCodesLocationName, JSON.stringify(t)), t
		}
		static get _shippingMethodLocationName() {
			return this._namespace + "_shipping_method_id"
		}
		static get _paymentProviderLocationName() {
			return this._namespace + "_payment_provider_name"
		}
		static get _paymentProviderParametersLocationName() {
			return this._namespace + "_payment_provider_parameters"
		}
		static get _discountCouponCodesLocationName() {
			return this._namespace + "_discount_coupon_codes"
		}
		static get _itemsLocationName() {
			return this._namespace + "_cart_items"
		}
		static get _redirectUrlLocationName() {
			return this._namespace + "_redirect_url"
		}
		static get _notesLocationName() {
			return this._namespace + "_notes"
		}
		static _contactInformationLocationName(t) {
			return this._namespace + "_address_data_" + t
		}
		static _generateuuid() {
			return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
		}
	}
	e.default = o, e.Cart = o, o._namespace = "plate_commerce_js"
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	});
	e.default = class {
		constructor(t) {
			this.items = [], this.items = t || [], this.couponCodes = []
		}
		asJson() {
			let t = {
				items: this.items,
				couponCodes: this.couponCodes
			};
			return this.shippingMethod && (t.shippingMethod = this.shippingMethod), this.paymentProvider && (t.paymentProvider = this.paymentProvider), this.paymentProviderParameters && (t.paymentProviderParameters = this.paymentProviderParameters), this.shippingAddress && (t.shippingAddress = this.shippingAddress), this.billingAddress && (t.billingAddress = this.billingAddress), this.email && (t.email = this.email), this.redirectUrl && (t.redirectUrl = this.redirectUrl), this.notes && (t.notes = this.notes), t
		}
	}
}, function(t, e, r) {
	"use strict";
	t.exports = function(t, e) {
		return function() {
			for (var r = new Array(arguments.length), n = 0; n < r.length; n++) r[n] = arguments[n];
			return t.apply(e, r)
		}
	}
}, function(t, e, r) {
	"use strict";
	var n = r(0);

	function o(t) {
		return encodeURIComponent(t).replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
	}
	t.exports = function(t, e, r) {
		if (!e) return t;
		var i;
		if (r) i = r(e);
		else if (n.isURLSearchParams(e)) i = e.toString();
		else {
			var s = [];
			n.forEach(e, (function(t, e) {
				null != t && (n.isArray(t) ? e += "[]" : t = [t], n.forEach(t, (function(t) {
					n.isDate(t) ? t = t.toISOString() : n.isObject(t) && (t = JSON.stringify(t)), s.push(o(e) + "=" + o(t))
				})))
			})), i = s.join("&")
		}
		if (i) {
			var a = t.indexOf("#"); - 1 !== a && (t = t.slice(0, a)), t += (-1 === t.indexOf("?") ? "?" : "&") + i
		}
		return t
	}
}, function(t, e, r) {
	"use strict";
	t.exports = function(t) {
		return !(!t || !t.__CANCEL__)
	}
}, function(t, e, r) {
	"use strict";
	(function(e) {
		var n = r(0),
			o = r(21),
			i = {
				"Content-Type": "application/x-www-form-urlencoded"
			};

		function s(t, e) {
			!n.isUndefined(t) && n.isUndefined(t["Content-Type"]) && (t["Content-Type"] = e)
		}
		var a, c = {
			adapter: (("undefined" != typeof XMLHttpRequest || void 0 !== e && "[object process]" === Object.prototype.toString.call(e)) && (a = r(7)), a),
			transformRequest: [function(t, e) {
				return o(e, "Accept"), o(e, "Content-Type"), n.isFormData(t) || n.isArrayBuffer(t) || n.isBuffer(t) || n.isStream(t) || n.isFile(t) || n.isBlob(t) ? t : n.isArrayBufferView(t) ? t.buffer : n.isURLSearchParams(t) ? (s(e, "application/x-www-form-urlencoded;charset=utf-8"), t.toString()) : n.isObject(t) ? (s(e, "application/json;charset=utf-8"), JSON.stringify(t)) : t
			}],
			transformResponse: [function(t) {
				if ("string" == typeof t) try {
					t = JSON.parse(t)
				} catch (t) {}
				return t
			}],
			timeout: 0,
			xsrfCookieName: "XSRF-TOKEN",
			xsrfHeaderName: "X-XSRF-TOKEN",
			maxContentLength: -1,
			maxBodyLength: -1,
			validateStatus: function(t) {
				return t >= 200 && t < 300
			}
		};
		c.headers = {
			common: {
				Accept: "application/json, text/plain, */*"
			}
		}, n.forEach(["delete", "get", "head"], (function(t) {
			c.headers[t] = {}
		})), n.forEach(["post", "put", "patch"], (function(t) {
			c.headers[t] = n.merge(i)
		})), t.exports = c
	}).call(this, r(20))
}, function(t, e, r) {
	"use strict";
	var n = r(0),
		o = r(22),
		i = r(24),
		s = r(4),
		a = r(25),
		c = r(28),
		u = r(29),
		d = r(8);
	t.exports = function(t) {
		return new Promise((function(e, r) {
			var l = t.data,
				f = t.headers;
			n.isFormData(l) && delete f["Content-Type"], (n.isBlob(l) || n.isFile(l)) && l.type && delete f["Content-Type"];
			var p = new XMLHttpRequest;
			if (t.auth) {
				var h = t.auth.username || "",
					m = unescape(encodeURIComponent(t.auth.password)) || "";
				f.Authorization = "Basic " + btoa(h + ":" + m)
			}
			var g = a(t.baseURL, t.url);
			if (p.open(t.method.toUpperCase(), s(g, t.params, t.paramsSerializer), !0), p.timeout = t.timeout, p.onreadystatechange = function() {
					if (p && 4 === p.readyState && (0 !== p.status || p.responseURL && 0 === p.responseURL.indexOf("file:"))) {
						var n = "getAllResponseHeaders" in p ? c(p.getAllResponseHeaders()) : null,
							i = {
								data: t.responseType && "text" !== t.responseType ? p.response : p.responseText,
								status: p.status,
								statusText: p.statusText,
								headers: n,
								config: t,
								request: p
							};
						o(e, r, i), p = null
					}
				}, p.onabort = function() {
					p && (r(d("Request aborted", t, "ECONNABORTED", p)), p = null)
				}, p.onerror = function() {
					r(d("Network Error", t, null, p)), p = null
				}, p.ontimeout = function() {
					var e = "timeout of " + t.timeout + "ms exceeded";
					t.timeoutErrorMessage && (e = t.timeoutErrorMessage), r(d(e, t, "ECONNABORTED", p)), p = null
				}, n.isStandardBrowserEnv()) {
				var y = (t.withCredentials || u(g)) && t.xsrfCookieName ? i.read(t.xsrfCookieName) : void 0;
				y && (f[t.xsrfHeaderName] = y)
			}
			if ("setRequestHeader" in p && n.forEach(f, (function(t, e) {
					void 0 === l && "content-type" === e.toLowerCase() ? delete f[e] : p.setRequestHeader(e, t)
				})), n.isUndefined(t.withCredentials) || (p.withCredentials = !!t.withCredentials), t.responseType) try {
				p.responseType = t.responseType
			} catch (e) {
				if ("json" !== t.responseType) throw e
			}
			"function" == typeof t.onDownloadProgress && p.addEventListener("progress", t.onDownloadProgress), "function" == typeof t.onUploadProgress && p.upload && p.upload.addEventListener("progress", t.onUploadProgress), t.cancelToken && t.cancelToken.promise.then((function(t) {
				p && (p.abort(), r(t), p = null)
			})), l || (l = null), p.send(l)
		}))
	}
}, function(t, e, r) {
	"use strict";
	var n = r(23);
	t.exports = function(t, e, r, o, i) {
		var s = new Error(t);
		return n(s, e, r, o, i)
	}
}, function(t, e, r) {
	"use strict";
	var n = r(0);
	t.exports = function(t, e) {
		e = e || {};
		var r = {},
			o = ["url", "method", "data"],
			i = ["headers", "auth", "proxy", "params"],
			s = ["baseURL", "transformRequest", "transformResponse", "paramsSerializer", "timeout", "timeoutMessage", "withCredentials", "adapter", "responseType", "xsrfCookieName", "xsrfHeaderName", "onUploadProgress", "onDownloadProgress", "decompress", "maxContentLength", "maxBodyLength", "maxRedirects", "transport", "httpAgent", "httpsAgent", "cancelToken", "socketPath", "responseEncoding"],
			a = ["validateStatus"];

		function c(t, e) {
			return n.isPlainObject(t) && n.isPlainObject(e) ? n.merge(t, e) : n.isPlainObject(e) ? n.merge({}, e) : n.isArray(e) ? e.slice() : e
		}

		function u(o) {
			n.isUndefined(e[o]) ? n.isUndefined(t[o]) || (r[o] = c(void 0, t[o])) : r[o] = c(t[o], e[o])
		}
		n.forEach(o, (function(t) {
			n.isUndefined(e[t]) || (r[t] = c(void 0, e[t]))
		})), n.forEach(i, u), n.forEach(s, (function(o) {
			n.isUndefined(e[o]) ? n.isUndefined(t[o]) || (r[o] = c(void 0, t[o])) : r[o] = c(void 0, e[o])
		})), n.forEach(a, (function(n) {
			n in e ? r[n] = c(t[n], e[n]) : n in t && (r[n] = c(void 0, t[n]))
		}));
		var d = o.concat(i).concat(s).concat(a),
			l = Object.keys(t).concat(Object.keys(e)).filter((function(t) {
				return -1 === d.indexOf(t)
			}));
		return n.forEach(l, u), r
	}
}, function(t, e, r) {
	"use strict";

	function n(t) {
		this.message = t
	}
	n.prototype.toString = function() {
		return "Cancel" + (this.message ? ": " + this.message : "")
	}, n.prototype.__CANCEL__ = !0, t.exports = n
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	});
	class n {
		static commerceConnectorPath(t) {
			return `${this.commerceConnectorUrl}${t}`
		}
	}
	e.default = n, n.commerceConnectorUrl = "https://commerce.plateapps.com", n.siteTranslationId = 0, n.siteTranslationLanguage = "nl"
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	}), e.Order = e.Config = e.OrderHandler = e.Cart = void 0;
	const n = r(1);
	e.Cart = n.default;
	const o = r(13);
	e.OrderHandler = o.default;
	const i = r(11);
	e.Config = i.default;
	const s = r(2);
	e.Order = s.default
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	});
	const n = r(14),
		o = r(1),
		i = r(11),
		s = r(32);
	e.default = class {
		static computeOrderFromCart() {
			let t = o.Cart.getOrder();
			return this.computeOrder(t)
		}
		static computeOrder(t) {
			let e = this.addConfigDataToOrder(t);
			return new Promise((t, r) => {
				n.post(i.default.commerceConnectorPath("/api/v1/orders/ecwid/compute"), {
					headers: {
						"Content-Type": "application/json"
					},
					data: e
				}).then(e => {
					t(new s.default(e.data))
				}).catch(t => {
					console.log("Error while trying to compute order in PlateCommerceJs:", t), r(t)
				})
			})
		}
		static placeOrderFromCart() {
			let t = o.Cart.getOrder(),
				e = this.addConfigDataToOrder(t);
			return new Promise((t, r) => {
				n.post(i.default.commerceConnectorPath("/api/v1/orders/ecwid/place"), {
					headers: {
						"Content-Type": "application/json"
					},
					data: e
				}).then(e => {
					t(e.data)
				}).catch(t => {
					console.error("Error while trying to place order in PlateCommerceJs:", t), r(t)
				})
			})
		}
		static addConfigDataToOrder(t) {
			return Object.assign({
				siteLanguage: i.default.siteTranslationLanguage,
				siteTranslationId: i.default.siteTranslationId
			}, t.asJson())
		}
	}
}, function(t, e, r) {
	t.exports = r(15)
}, function(t, e, r) {
	"use strict";
	var n = r(0),
		o = r(3),
		i = r(16),
		s = r(9);

	function a(t) {
		var e = new i(t),
			r = o(i.prototype.request, e);
		return n.extend(r, i.prototype, e), n.extend(r, e), r
	}
	var c = a(r(6));
	c.Axios = i, c.create = function(t) {
		return a(s(c.defaults, t))
	}, c.Cancel = r(10), c.CancelToken = r(30), c.isCancel = r(5), c.all = function(t) {
		return Promise.all(t)
	}, c.spread = r(31), t.exports = c, t.exports.default = c
}, function(t, e, r) {
	"use strict";
	var n = r(0),
		o = r(4),
		i = r(17),
		s = r(18),
		a = r(9);

	function c(t) {
		this.defaults = t, this.interceptors = {
			request: new i,
			response: new i
		}
	}
	c.prototype.request = function(t) {
		"string" == typeof t ? (t = arguments[1] || {}).url = arguments[0] : t = t || {}, (t = a(this.defaults, t)).method ? t.method = t.method.toLowerCase() : this.defaults.method ? t.method = this.defaults.method.toLowerCase() : t.method = "get";
		var e = [s, void 0],
			r = Promise.resolve(t);
		for (this.interceptors.request.forEach((function(t) {
				e.unshift(t.fulfilled, t.rejected)
			})), this.interceptors.response.forEach((function(t) {
				e.push(t.fulfilled, t.rejected)
			})); e.length;) r = r.then(e.shift(), e.shift());
		return r
	}, c.prototype.getUri = function(t) {
		return t = a(this.defaults, t), o(t.url, t.params, t.paramsSerializer).replace(/^\?/, "")
	}, n.forEach(["delete", "get", "head", "options"], (function(t) {
		c.prototype[t] = function(e, r) {
			return this.request(a(r || {}, {
				method: t,
				url: e
			}))
		}
	})), n.forEach(["post", "put", "patch"], (function(t) {
		c.prototype[t] = function(e, r, n) {
			return this.request(a(n || {}, {
				method: t,
				url: e,
				data: r
			}))
		}
	})), t.exports = c
}, function(t, e, r) {
	"use strict";
	var n = r(0);

	function o() {
		this.handlers = []
	}
	o.prototype.use = function(t, e) {
		return this.handlers.push({
			fulfilled: t,
			rejected: e
		}), this.handlers.length - 1
	}, o.prototype.eject = function(t) {
		this.handlers[t] && (this.handlers[t] = null)
	}, o.prototype.forEach = function(t) {
		n.forEach(this.handlers, (function(e) {
			null !== e && t(e)
		}))
	}, t.exports = o
}, function(t, e, r) {
	"use strict";
	var n = r(0),
		o = r(19),
		i = r(5),
		s = r(6);

	function a(t) {
		t.cancelToken && t.cancelToken.throwIfRequested()
	}
	t.exports = function(t) {
		return a(t), t.headers = t.headers || {}, t.data = o(t.data, t.headers, t.transformRequest), t.headers = n.merge(t.headers.common || {}, t.headers[t.method] || {}, t.headers), n.forEach(["delete", "get", "head", "post", "put", "patch", "common"], (function(e) {
			delete t.headers[e]
		})), (t.adapter || s.adapter)(t).then((function(e) {
			return a(t), e.data = o(e.data, e.headers, t.transformResponse), e
		}), (function(e) {
			return i(e) || (a(t), e && e.response && (e.response.data = o(e.response.data, e.response.headers, t.transformResponse))), Promise.reject(e)
		}))
	}
}, function(t, e, r) {
	"use strict";
	var n = r(0);
	t.exports = function(t, e, r) {
		return n.forEach(r, (function(r) {
			t = r(t, e)
		})), t
	}
}, function(t, e) {
	var r, n, o = t.exports = {};

	function i() {
		throw new Error("setTimeout has not been defined")
	}

	function s() {
		throw new Error("clearTimeout has not been defined")
	}

	function a(t) {
		if (r === setTimeout) return setTimeout(t, 0);
		if ((r === i || !r) && setTimeout) return r = setTimeout, setTimeout(t, 0);
		try {
			return r(t, 0)
		} catch (e) {
			try {
				return r.call(null, t, 0)
			} catch (e) {
				return r.call(this, t, 0)
			}
		}
	}! function() {
		try {
			r = "function" == typeof setTimeout ? setTimeout : i
		} catch (t) {
			r = i
		}
		try {
			n = "function" == typeof clearTimeout ? clearTimeout : s
		} catch (t) {
			n = s
		}
	}();
	var c, u = [],
		d = !1,
		l = -1;

	function f() {
		d && c && (d = !1, c.length ? u = c.concat(u) : l = -1, u.length && p())
	}

	function p() {
		if (!d) {
			var t = a(f);
			d = !0;
			for (var e = u.length; e;) {
				for (c = u, u = []; ++l < e;) c && c[l].run();
				l = -1, e = u.length
			}
			c = null, d = !1,
				function(t) {
					if (n === clearTimeout) return clearTimeout(t);
					if ((n === s || !n) && clearTimeout) return n = clearTimeout, clearTimeout(t);
					try {
						n(t)
					} catch (e) {
						try {
							return n.call(null, t)
						} catch (e) {
							return n.call(this, t)
						}
					}
				}(t)
		}
	}

	function h(t, e) {
		this.fun = t, this.array = e
	}

	function m() {}
	o.nextTick = function(t) {
		var e = new Array(arguments.length - 1);
		if (arguments.length > 1)
			for (var r = 1; r < arguments.length; r++) e[r - 1] = arguments[r];
		u.push(new h(t, e)), 1 !== u.length || d || a(p)
	}, h.prototype.run = function() {
		this.fun.apply(null, this.array)
	}, o.title = "browser", o.browser = !0, o.env = {}, o.argv = [], o.version = "", o.versions = {}, o.on = m, o.addListener = m, o.once = m, o.off = m, o.removeListener = m, o.removeAllListeners = m, o.emit = m, o.prependListener = m, o.prependOnceListener = m, o.listeners = function(t) {
		return []
	}, o.binding = function(t) {
		throw new Error("process.binding is not supported")
	}, o.cwd = function() {
		return "/"
	}, o.chdir = function(t) {
		throw new Error("process.chdir is not supported")
	}, o.umask = function() {
		return 0
	}
}, function(t, e, r) {
	"use strict";
	var n = r(0);
	t.exports = function(t, e) {
		n.forEach(t, (function(r, n) {
			n !== e && n.toUpperCase() === e.toUpperCase() && (t[e] = r, delete t[n])
		}))
	}
}, function(t, e, r) {
	"use strict";
	var n = r(8);
	t.exports = function(t, e, r) {
		var o = r.config.validateStatus;
		r.status && o && !o(r.status) ? e(n("Request failed with status code " + r.status, r.config, null, r.request, r)) : t(r)
	}
}, function(t, e, r) {
	"use strict";
	t.exports = function(t, e, r, n, o) {
		return t.config = e, r && (t.code = r), t.request = n, t.response = o, t.isAxiosError = !0, t.toJSON = function() {
			return {
				message: this.message,
				name: this.name,
				description: this.description,
				number: this.number,
				fileName: this.fileName,
				lineNumber: this.lineNumber,
				columnNumber: this.columnNumber,
				stack: this.stack,
				config: this.config,
				code: this.code
			}
		}, t
	}
}, function(t, e, r) {
	"use strict";
	var n = r(0);
	t.exports = n.isStandardBrowserEnv() ? {
		write: function(t, e, r, o, i, s) {
			var a = [];
			a.push(t + "=" + encodeURIComponent(e)), n.isNumber(r) && a.push("expires=" + new Date(r).toGMTString()), n.isString(o) && a.push("path=" + o), n.isString(i) && a.push("domain=" + i), !0 === s && a.push("secure"), document.cookie = a.join("; ")
		},
		read: function(t) {
			var e = document.cookie.match(new RegExp("(^|;\\s*)(" + t + ")=([^;]*)"));
			return e ? decodeURIComponent(e[3]) : null
		},
		remove: function(t) {
			this.write(t, "", Date.now() - 864e5)
		}
	} : {
		write: function() {},
		read: function() {
			return null
		},
		remove: function() {}
	}
}, function(t, e, r) {
	"use strict";
	var n = r(26),
		o = r(27);
	t.exports = function(t, e) {
		return t && !n(e) ? o(t, e) : e
	}
}, function(t, e, r) {
	"use strict";
	t.exports = function(t) {
		return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(t)
	}
}, function(t, e, r) {
	"use strict";
	t.exports = function(t, e) {
		return e ? t.replace(/\/+$/, "") + "/" + e.replace(/^\/+/, "") : t
	}
}, function(t, e, r) {
	"use strict";
	var n = r(0),
		o = ["age", "authorization", "content-length", "content-type", "etag", "expires", "from", "host", "if-modified-since", "if-unmodified-since", "last-modified", "location", "max-forwards", "proxy-authorization", "referer", "retry-after", "user-agent"];
	t.exports = function(t) {
		var e, r, i, s = {};
		return t ? (n.forEach(t.split("\n"), (function(t) {
			if (i = t.indexOf(":"), e = n.trim(t.substr(0, i)).toLowerCase(), r = n.trim(t.substr(i + 1)), e) {
				if (s[e] && o.indexOf(e) >= 0) return;
				s[e] = "set-cookie" === e ? (s[e] ? s[e] : []).concat([r]) : s[e] ? s[e] + ", " + r : r
			}
		})), s) : s
	}
}, function(t, e, r) {
	"use strict";
	var n = r(0);
	t.exports = n.isStandardBrowserEnv() ? function() {
		var t, e = /(msie|trident)/i.test(navigator.userAgent),
			r = document.createElement("a");

		function o(t) {
			var n = t;
			return e && (r.setAttribute("href", n), n = r.href), r.setAttribute("href", n), {
				href: r.href,
				protocol: r.protocol ? r.protocol.replace(/:$/, "") : "",
				host: r.host,
				search: r.search ? r.search.replace(/^\?/, "") : "",
				hash: r.hash ? r.hash.replace(/^#/, "") : "",
				hostname: r.hostname,
				port: r.port,
				pathname: "/" === r.pathname.charAt(0) ? r.pathname : "/" + r.pathname
			}
		}
		return t = o(window.location.href),
			function(e) {
				var r = n.isString(e) ? o(e) : e;
				return r.protocol === t.protocol && r.host === t.host
			}
	}() : function() {
		return !0
	}
}, function(t, e, r) {
	"use strict";
	var n = r(10);

	function o(t) {
		if ("function" != typeof t) throw new TypeError("executor must be a function.");
		var e;
		this.promise = new Promise((function(t) {
			e = t
		}));
		var r = this;
		t((function(t) {
			r.reason || (r.reason = new n(t), e(r.reason))
		}))
	}
	o.prototype.throwIfRequested = function() {
		if (this.reason) throw this.reason
	}, o.source = function() {
		var t;
		return {
			token: new o((function(e) {
				t = e
			})),
			cancel: t
		}
	}, t.exports = o
}, function(t, e, r) {
	"use strict";
	t.exports = function(t) {
		return function(e) {
			return t.apply(null, e)
		}
	}
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	});
	const n = r(33);
	e.default = class {
		constructor(t) {
			this.items = [];
			let e = t.data;
			e.items.forEach(t => {
				this.items.push(new n.default(t))
			}), this._setAttributes(e)
		}
		_setAttributes(t) {
			["subtotal", "totalTaxes", "total", "subtotalWithTaxes", "discountCoupons", "ignoredDiscountCoupons", "discount", "total", "shippingMethod"].forEach(e => this[e] = t[e])
		}
	}
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	});
	const n = r(34);
	class o extends n.default {
		constructor(t) {
			super(t), ["price", "subtotal", "taxes", "total", "wholesaleDiscount", "compareToPrice"].forEach(e => this[e] = t[e])
		}
	}
	e.default = o
}, function(t, e, r) {
	"use strict";
	Object.defineProperty(e, "__esModule", {
		value: !0
	});
	e.default = class {
		constructor(t) {
			["uuid", "quantity", "selectedOptions", "productVariationId"].forEach(e => this[e] = t[e]), this.id = t.productId
		}
	}
}]);
